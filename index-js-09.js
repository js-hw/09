function createList(massive, parent = document.body) {
  let details = document.createElement("details");
  details.id = `${parent}`;
  details.classList.add("bardo");
  details.innerHTML = "<summary>4. Практичне завдання: список із масиву.</summary>";
  document.body.append(details);

  let ul = document.createElement("ul");
  ul.style.margin = "0 200px";

  let arr = massive.split(/[,/&@:;/\\.#?]+/);

  arr.forEach((item) => {
    let li = document.createElement("li");
    li.innerText = item;
    ul.append(li);
  });
  
  let ulPosition = document.querySelector(`#${parent}`)
  ulPosition.append(ul);
}

function checkPrompt(checker) {
  while (true) {
    if (checker === null) {
      return "Escaped! You have given up the option to create the array";
    } else if (checker == "") {
      checker = prompt("There are no values. You've entered nothing. Please, enter new array", "Cars/ Porche@Hundai & Mazda: 12, Something else! New cars and more\\BMW");
    } else return checker;
  }
}

let array = prompt("Enter any array:", "Hello, World, Kiev, Kharkiv, Odessa, Lviv");
let param = "practice";

let newWorld = checkPrompt(array);
createList(newWorld, param);